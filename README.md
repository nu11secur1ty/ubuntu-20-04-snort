# ubuntu-20.04-snort

![](https://github.com/nu11secur1ty/ubuntu-20.04-snort/blob/master/logo/586e6b2cc2d41da57a33ca0d.png)

Snort++ build quick start (inside the container) with unit test support:

- Loading and login:

```bash
docker run -t -i nu11secur1ty/ubuntu-20.04-snort /bin/bash
```
- Direct install

```bash
apt install snort -y
```
- Follow the instructions:

![](https://github.com/nu11secur1ty/ubuntu-20.04-snort/blob/master/logo/Screenshot%20from%202020-05-13%2022-58-46.png)

![](https://github.com/nu11secur1ty/ubuntu-20.04-snort/blob/master/logo/Screenshot%20from%202020-05-13%2022-59-25.png)

- Snort3 -DEV...

```
cd $HOME
git clone https://github.com/snort3/libdaq.git
cd libdaq
./bootstrap
./configure
make install
ldconfig

cd $HOME
git clone https://github.com/snort3/snort3.git
cd snort3
./configure\_cmake.sh --prefix=$HOME/install/snort3 --enable-unit-tests
cd build
make -j$(nproc) install
make -j$(nproc) check

$HOME/install/snort3/bin/snort -V
$HOME/install/snort3/bin/snort --catch-test all
```
